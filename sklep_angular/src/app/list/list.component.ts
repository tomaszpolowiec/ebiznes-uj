import { Component, OnInit } from '@angular/core';
import {ProductService} from "../product/product.service";
import {CategoryService} from "../category/category.service";
import {Product} from "../product/product";
import {Category} from "../category/category";
import {BasketService} from "../basket/basket.service";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  providers: [ProductService, CategoryService]
})
export class ListComponent implements OnInit {

  products: Product[]= [];
  categories: Category[] = [];

  constructor(private productService: ProductService, private categoryService: CategoryService, public basketService: BasketService)
  {
    this.productService.getProducts().subscribe(data => this.products= data.json());
    this.categoryService.getCategories().subscribe(data => this.categories= data.json());

  }

  addToBasket(prodId: number)
  {
    this.basketService.addProduct(prodId);
  }

  ngOnInit() {
  }


}
