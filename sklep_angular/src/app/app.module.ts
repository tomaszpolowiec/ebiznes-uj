import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { ProductComponent } from './product/product.component';
import {RouterModule} from '@angular/router';
import { CategoryComponent } from './category/category.component';

import * as $ from 'jquery';
import { ListComponent } from './list/list.component';
import { UserComponent } from './user/user.component';
import { LoginComponent } from './login/login.component';
import { BasketComponent } from './basket/basket.component';
import {BasketService} from "./basket/basket.service";
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { PurchaseComponent } from './purchase/purchase.component';
import { PurchaselistComponent } from './purchaselist/purchaselist.component';

@NgModule({
  declarations: [
    AppComponent,
    ProductComponent,
    CategoryComponent,
    ListComponent,
    UserComponent,
    LoginComponent,
    BasketComponent,
    PurchaseComponent,
    PurchaselistComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    RouterModule.forRoot([
      {path: '', component: LoginComponent},
      {path: 'products', component: ProductComponent },
      {path: 'categories', component: CategoryComponent },
      {path: 'users', component: UserComponent },
      {path: 'list', component: ListComponent },
      {path: 'basket', component: BasketComponent },
      {path: 'purchase', component: PurchaseComponent },
      {path: 'purchaselist', component: PurchaselistComponent}

    ])
  ],
  providers: [BasketService, CookieService],
  bootstrap: [AppComponent]
})

export class AppModule { }
