import { Component, OnInit } from '@angular/core';
import {BasketService} from "./basket.service";
import {Product} from "../product/product";
import {ProductService} from "../product/product.service";

@Component({
  selector: 'app-basket',
  templateUrl: './basket.component.html',
  styleUrls: ['./basket.component.css'],
  providers: [ProductService]
})
export class BasketComponent implements OnInit {

  products: Product[] = [];

  constructor(private basketService: BasketService, private productService: ProductService)
  {
    this.productService.getProducts().subscribe(data => this.products= data.json() );
  }

  numberInBasket(prodId: number)
  {
    var val = this.basketService.productsInBasket[prodId];
    if(val > 0)
      return val;
    return 0;
  }

  priceForProduct(prodId: number)
  {
    var price= 0;
    for(var prod of this.products)
    {
      if(prod.prodId == prodId)
      {
        price= prod.cena;
        break;
      }
    }
    return (price * this.numberInBasket(prodId)).toFixed(2);
  }

  sumOfPrice()
  {
    var price= 0;
    for(var prod of this.products)
    {

        price += prod.cena*this.numberInBasket(prod.prodId);

    }
    return price.toFixed(2);
  }

  ngOnInit() {
  }

}
