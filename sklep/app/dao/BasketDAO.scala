package dao

import javax.inject.Inject

import models.{ Basket, BasketREST }
import play.api.db.slick.{ DatabaseConfigProvider, HasDatabaseConfigProvider }
import slick.driver.MySQLDriver

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{ ExecutionContext, Future }

/**
 * Created by Tomek93 on 2017-06-12.
 */
class BasketDAO @Inject() (protected val dbConfigProvider: DatabaseConfigProvider)
    extends HasDatabaseConfigProvider[MySQLDriver] {

  import driver.api._

  val Baskets = TableQuery[CategoriesTable]

  def all: Future[List[BasketREST]] = {
    val query = Baskets
    val results = query.result
    val futureBaskets = db.run(results)
    futureBaskets.map(
      _.map {
      a => BasketREST(userId = a.userId, jsonProducts = a.jsonProducts)
    }.toList
    )
  }

  def getBasketByUserId(userId: String): Future[List[BasketREST]] = {
    val query = Baskets.filter(_.userId === userId)
    val results = query.result
    val futureBaskets = db.run(results)
    futureBaskets.map(
      _.map {
      a => BasketREST(userId = a.userId, jsonProducts = a.jsonProducts)
    }.toList
    )
  }

  def insert(basket: Basket): Future[BasketREST] = {

    this.deleteByUserId(basket.userId)

    val insertQuery = Baskets returning Baskets.map(_.id) into ((basket, id) => basket.copy(id = id))
    val action = insertQuery += basket
    db.run(action).map(a => BasketREST(userId = a.userId, jsonProducts = a.jsonProducts))
  }

  def deleteByUserId(userId: String): Future[Int] = db.run(Baskets.filter(_.userId === userId).delete)

  class CategoriesTable(tag: Tag) extends Table[Basket](tag, "koszyk") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def userId = column[String]("userId")
    def jsonProducts = column[String]("jsonProducts")
    def * = (id, userId, jsonProducts) <> (models.Basket.tupled, models.Basket.unapply)
  }
}
