package models

import play.api.libs.json.Json

/**
 * Created by Tomek93 on 2017-06-10.
 */
case class RegisteredUserREST(userId: String, provider: String, firstName: String, lastName: String, fullName: String, email: String, avatarUrl: String, isAdmin: Int)

object RegisteredUserREST {
  implicit val productFormat = Json.format[RegisteredUserREST]
}