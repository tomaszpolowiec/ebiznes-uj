package models

/**
 * Created by Tomek93 on 2017-06-12.
 */
case class Basket(id: Long, userId: String, jsonProducts: String)
