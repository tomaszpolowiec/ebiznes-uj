package models

import play.api.libs.json.Json

/**
 * Created by Tomek93 on 2017-06-12.
 */
case class BasketREST(userId: String, jsonProducts: String)

object BasketREST {
  implicit val productFormat = Json.format[BasketREST]
}
