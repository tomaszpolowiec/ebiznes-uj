package models
import java.sql.Timestamp
import play.api.libs.json.Format
/**
 * Created by tomek on 11.04.17.
 */

case class Product(prodId: Long, tytul: String, opis: String, img: String, cena: Double, catId: Long)