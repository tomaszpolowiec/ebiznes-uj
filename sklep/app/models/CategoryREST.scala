package models

import play.api.libs.json.Json

/**
 * Created by Tomek93 on 2017-05-28.
 */
case class CategoryREST(catId: Long, nazwa: String)

object CategoryREST {
  implicit val productFormat = Json.format[CategoryREST]
}
