package models

import play.api.libs.json.Json

/**
 * Created by Tomek93 on 2017-06-12.
 */
case class PurchaseREST(id: Long, userId: String, adress: String, sendType: String, jsonOrder: String, totalPrice: Double, date: String)

object PurchaseREST {
  implicit val productFormat = Json.format[PurchaseREST]
}

