package models

import play.api.libs.json.Json

/**
 * Created by Tomek93 on 2017-05-13.
 */
case class ProductREST(prodId: Long, tytul: String, opis: String, img: String, cena: Double, catId: Long)

object ProductREST {
  implicit val productFormat = Json.format[ProductREST]
}
