package models

import java.sql.Timestamp

/**
 * Created by Tomek93 on 2017-06-12.
 */
case class Purchase(id: Long, userId: String, adress: String, sendType: String, jsonOrder: String, totalPrice: Double, date: String)
