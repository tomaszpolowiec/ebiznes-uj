package models

/**
 * Created by Tomek93 on 2017-06-10.
 */
case class RegisteredUser(id: Long, userId: String, provider: String, firstName: String, lastName: String, fullName: String, email: String, avatarUrl: String, isAdmin: Int)

