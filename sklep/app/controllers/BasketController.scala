package controllers

import javax.inject.Inject

import dao.BasketDAO
import models.{ Basket, BasketREST }
import play.api.libs.json.Json
import play.api.mvc.{ Action, Controller }
import scala.concurrent.ExecutionContext.Implicits.global

/**
 * Created by Tomek93 on 2017-06-12.
 */
class BasketController @Inject() (basketDAO: BasketDAO) extends Controller {

  def getById(userId: String) = Action.async { implicit request =>
    basketDAO.getBasketByUserId(userId) map {
      baskets => Ok(Json.toJson(baskets))
    }
  }

  def newBasket = Action.async { implicit request =>
    var json: BasketREST = request.body.asJson.get.as[BasketREST]
    var basket = Basket(id = 0, userId = json.userId, jsonProducts = json.jsonProducts)
    var basketResult = basketDAO.insert(basket)
    basketResult.map {
      basket => Ok(Json.toJson(basket))
    }
  }

  def deleteBasket(userId: String) = Action { implicit request =>
    basketDAO.deleteByUserId(userId)
    Ok("OK")
  }
}
