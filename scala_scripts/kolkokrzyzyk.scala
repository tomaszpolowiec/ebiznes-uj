import scala.io.{Source, StdIn}

/**
  * Created by tomek on 20.03.17.
  */

var area = new Array[String](9)
for( i <- 0 until area.length)
{
  area(i)= " "
}

def printArea: Unit =
{
  println( "0|1|2  "+ area(0) +"|" +area(1) +"|" +area(2) )
  println( "-----  -----")
  println( "3|4|5  "+ area(3) +"|" +area(4) +"|" +area(5) )
  println( "-----  -----")
  println( "6|7|8  "+ area(6) +"|" +area(7) +"|" +area(8) )

}

def checkWinner: String =
{
  //check O
  var signCheck = "X";
  for( i <- 0 until 3)
  {
    if( area(i*3 + 0) == signCheck && area(i*3 + 1) == signCheck && area(i*3 + 2) == signCheck )
    {
      return signCheck;
    }
    if( area(i + 0) == signCheck && area(i + 3) == signCheck && area(i + 6) == signCheck )
    {
      return signCheck;
    }

  }
  if( area(0) == signCheck && area(4) == signCheck && area(8) == signCheck )
  {
    return signCheck;
  }
  if( area(2) == signCheck && area(4) == signCheck && area(6) == signCheck )
  {
    return signCheck;
  }

  signCheck = "O";
  for( i <- 0 until 3)
  {
    if( area(i*3 + 0) == signCheck && area(i*3 + 1) == signCheck && area(i*3 + 2) == signCheck )
    {
      return signCheck;
    }
    if( area(i + 0) == signCheck && area(i + 3) == signCheck && area(i + 6) == signCheck )
    {
      return signCheck;
    }

  }
  if( area(0) == signCheck && area(4) == signCheck && area(8) == signCheck )
  {
    return signCheck;
  }
  if( area(2) == signCheck && area(4) == signCheck && area(6) == signCheck )
  {
    return signCheck;
  }

  return "";
}

printArea

var symbol:Boolean = true
var tmp:String = "O"

var i = 0;
while( i < 9)
{
  var input = -1;
  var cond = true;
  while(cond)
  {
    System.out.println(tmp + " wprowadz index")
    input = StdIn.readInt()
    if( input < 0 || input >= 9 || area(input) != " ")
    {
      println("Niepoprawny index");
    }
    else
    {
      cond=false;
    }
  }

  if( symbol == true )
  {
    area(input) = "O"
    tmp= "X"
  }
  else
  {
    area(input) = "X"
    tmp="O"
  }
  symbol = !symbol
  printArea

  if( checkWinner != "" )
  {
    i= 100;
  }
  i= i+1;
}

var winner = checkWinner;
if(winner == "")
{
  println("Remis");
}
else
{
  println("Wygral "+winner);
}
