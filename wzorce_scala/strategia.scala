import scala.io.Source

/**
  * Created by tomek on 10.04.17.
  */

abstract class ReadStrategy
{
  def getInput():String;
};

class FileReadStrategy(fileName:String) extends ReadStrategy
{
  override def getInput():String =
  {
    var str = "";
    var file = Source.fromFile(fileName);
    while(file.hasNext)
    {
      str+= file.next();
    }
    return str;
  }
}

class KeyboardReadStrategy extends ReadStrategy
{
  override def getInput():String =
  {
    var str = scala.io.StdIn.readLine();
    return str;
  }
}

abstract class CharStrategy
{
  def filter(text:String):String;
}

class UppercaseCharStrategy extends CharStrategy
{
  override def filter(text: String):String =
  {
    return text.toUpperCase;
  }
}

class WhitespaceCleanCharStrategy extends CharStrategy
{
  override def filter(text: String):String =
  {
    return text.filterNot((x: Char) => x.isWhitespace);
  }
}

class Reader(readStrategyC: ReadStrategy, charStrategyC: CharStrategy)
{
  var readStrategy = readStrategyC;
  var charStrategy = charStrategyC;

  def setReadStrategy(readStrategy: ReadStrategy): Unit =
  {
    this.readStrategy= readStrategy;
  }

  def setCharStrategy(charStrategy: CharStrategy): Unit =
  {
    this.charStrategy = charStrategy;
  }

  def read(): Unit =
  {
    System.out.println(charStrategy.filter(readStrategy.getInput()));
  }
}

var r = new Reader(new KeyboardReadStrategy, new WhitespaceCleanCharStrategy);
r.read();
r.setCharStrategy(new UppercaseCharStrategy);
r.read();