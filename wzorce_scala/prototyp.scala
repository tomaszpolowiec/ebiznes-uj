import scala.io.Source

/**
  * Created by tomek on 04.04.17.
  */

class Reader
{
  def getInput(): String =
  {
    return "";
  }

  override def clone(): Reader = new Reader
}

class InputReader extends Reader
{
  override def getInput(): String =
  {
    var str = scala.io.StdIn.readLine();
    return str;
  }

  def read(): Unit =
  {
    System.out.println(this.getInput());
  }

  override def clone(): InputReader = new InputReader
}

class WhitespaceCleanInputReader extends InputReader
{
  override def read(): Unit =
  {
    var str = this.getInput().filterNot((x: Char) => x.isWhitespace);
    System.out.println(str);
  }

  override def clone(): WhitespaceCleanInputReader = new WhitespaceCleanInputReader
}

class UppercaseInputReader extends InputReader
{
  override def read(): Unit =
  {
    var str = this.getInput().toUpperCase;
    System.out.println(str);
  }

  override def clone(): UppercaseInputReader = new UppercaseInputReader
}

class FileReader extends Reader
{
  def getInput(fileName: String): String =
  {
    var str = "";
    var file = Source.fromFile(fileName);
    while(file.hasNext)
    {
      str+= file.next();
    }
    return str;
  }

  override def clone(): FileReader = new FileReader
}

class WhitespaceCleanFileReader extends FileReader
{
  def read(fileName: String): Unit =
  {
    var str = this.getInput(fileName).filterNot((x: Char) => x.isWhitespace);
    System.out.println(str);
  }

  override def clone(): WhitespaceCleanFileReader = new WhitespaceCleanFileReader
}

class UppercaseFileReader extends FileReader
{
  def read(fileName: String): Unit =
  {
    var str = this.getInput(fileName).toUpperCase;
    System.out.println(str);
  }

  override def clone(): UppercaseFileReader = new UppercaseFileReader
}

//var r = new UppercaseInputReader();
//r.read();
var fr = new UppercaseFileReader();
var fr2 = fr.clone();
fr2.read("/home/tomek/IdeaProjects/Scala1/src/wzorce_scala/dekorator/dekorator.scala");