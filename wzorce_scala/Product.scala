package models
import play.api.libs.json._
import zamblauskas.csv.parser._
/**
  * Created by tomek on 11.04.17.
  */

class ClonableProduct
{
  override def clone(): AnyRef = super.clone()
}

case class ProductPrototype(id: Int, name: String, description: String, price: Double) extends ClonableProduct
{

  def getId() : Int = id;

  def getName() : String = name;

  def getDescription() : String = description;

  def getPrice() : Double = price;

  override def clone(): ProductPrototype =
  {
    return new ProductPrototype(id, name, description, price);
  }
}

object ProductPrototype {
  implicit val productsFormat = Json.format[ProductPrototype]

  def mapperTo(
                id: Int, name: String, description: String, price: Double
              ) = apply(id, name, description, price)
}

abstract class ParserStrategy
{
  def parse(string: String):Any;
}

class JsonParser extends ParserStrategy
{
  override def parse(jsonString: String): Any =
  {
    return Json.parse(jsonString);
  }
}


class CVSParser extends ParserStrategy
{
  override def parse(string: String): Any =
  {
    return Parser.parse[ProductPrototype](csv);
  }
}

class StringParser(parserStrategyC: ParserStrategy)
{
  var parserStrategy: ParserStrategy= parserStrategyC;

  def setStrategy(parserStrategy: ParserStrategy): Unit =
  {
    this.parserStrategy = parserStrategy;
  }

  def parse(string:String) : Any =
  {
    return parserStrategy.parse(string);
  }

}