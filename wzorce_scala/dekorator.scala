import scala.io.Source

/**
  * Created by tomek on 04.04.17.
  */

class Reader
{
    def getInput(): String =
    {
        return "";
    }
}

class InputReader(r: Reader)
{
    def getInput(): String =
    {
      var str = scala.io.StdIn.readLine();
      return str;
    }

    def read(): Unit =
    {
        System.out.println(this.getInput());
    }
}

class WhitespaceCleanInputReader(ir: InputReader)
{
    def read(): Unit =
    {
        var str = ir.getInput().filterNot((x: Char) => x.isWhitespace);
        System.out.println(str);
    }
}

class UppercaseInputReader(ir: InputReader)
{
    def read(): Unit =
    {
        var str = ir.getInput().toUpperCase;
        System.out.println(str);
    }
}

class FileReader(r: Reader)
{
    def getInput(fileName: String): String =
    {
        var str = "";
        var file = Source.fromFile(fileName);
        while(file.hasNext)
        {
            str+= file.next();
        }
        return str;
    }

    def read(fileName: String): Unit =
    {
        System.out.println(this.getInput(fileName));
    }
}

class WhitespaceCleanFileReader(fr: FileReader)
{
    def read(fileName: String): Unit =
    {
        var str = fr.getInput(fileName).filterNot((x: Char) => x.isWhitespace);
        System.out.println(str);
    }
}

class UppercaseFileReader(fr: FileReader)
{
    def read(fileName: String): Unit =
    {
        var str = fr.getInput(fileName).toUpperCase;
        System.out.println(str);
    }
}

//var r = new UppercaseInputReader(new InputReader(new Reader));
//r.read();
var fr = new UppercaseFileReader(new FileReader(new Reader));
fr.read("/home/tomek/IdeaProjects/Scala1/src/wzorce_scala/dekorator/dekorator.scala");
